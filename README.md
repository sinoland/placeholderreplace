#How to use
The executable jar file is located in release folder, make sure you have installed JRE (Java SE Runtime Environment) or JDK (Java SE Development Kit) 1.8, i.e. Java 8 or above.

Execute:

`java -jar placeholder-replace-runnable.jar`

You will see:

`Usage: <path-of-properties-file> <path-of-inspected-source-dir> [-e/i] [file-regex-pattern]`  
`Mandantory: <path-of-properties-file>`  
`Mandantory: <path-of-inspected-source-dir>`  
`Optional: [-e/i] - the matches to be included or exclude for inspection. Default is exclude.`  
`Optional: [exclude-file-regex-pattern]`  


Below are the default exculde file pattern (case insensitive).

1. File extension  
*.bak
*.bak\\d+
*.rej
*.db
*.gif
*.jpg
*.pdf
*.png
*.exe
*.cab
*.dll
*.jar
*.zip
*.7z
*.sql
*.db
*.swf
*.rpt
*.ori
*.dep
*.cab
*.ocx
*.ds_store
*.gitignore

2. Filename  
*backup*


3. File Directory  
.git  
project_images  
sql  
fonts  
bin  


Below is the default inculde file pattern.
*.txt


#How to modify/develop:
Make your change (likely in PlaceholderReplace.java), update the document in README.md (this file). Then in Eclipse, File->Export->Runnable JAR file.