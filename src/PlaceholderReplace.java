import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PlaceholderReplace {
	public static final String exculdeFileExtRegexStr = ".*\\.(bak|bak\\d+|rej|db|gif|jpg|pdf|png|exe|cab|dll|jar|zip|7z|sql|db|swf|rpt|ori|dep|cab|ocx|ds_store|gitignore)";
	public static final String exculdeFilenameRegexStr = ".*(backup).*";
	public static final String exculdeFileDirRegexStr = ".*(\\\\|/)(\\.git|project_images|sql|fonts|bin)(\\\\|/).*";
	public static final String defaultExculdeFileRegexStr = "(?i)(" + exculdeFileExtRegexStr + "|" + exculdeFilenameRegexStr + "|" + exculdeFileDirRegexStr + ")";
	public static final String defaultIncludeFileRegexStr = ".*\\.(txt)";
			
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length > 4 || args.length < 2) {
			System.err.println("Usage: <path-of-properties-file> <path-of-inspected-source-dir> [-e/i] [file-regex-pattern]");
			System.err.println("Mandantory: <path-of-properties-file>");
			System.err.println("Mandantory: <path-of-inspected-source-dir>");
			System.err.println("Optional: [-e/i] - the matches to be included or exclude for inspection. Default is exclude.");
			System.err.println("Optional: [exclude-file-regex-pattern]");
			System.exit(-1);
		}
		
		if (args.length > 2 && args[2].equals("-i") && args[2].equals("-e")) {
			System.err.println("The option is not correct, must be -e or -i!");
			System.exit(-1);
		}
		
		final Path propertiesFilePath = Paths.get(args[0]);
		final Path inspectSourceFileDirPath = Paths.get(args[1]);
		final Boolean isExclude = args.length >= 3 && args[2].equals("-i") ? false : true;  
		final String excluldeFileRegexString = args.length == 4 && isExclude ? args[3] : defaultExculdeFileRegexStr; 
		final String incluldeFileRegexString = args.length == 4 && !isExclude ? args[3] : defaultIncludeFileRegexStr; 
		
		if (Files.notExists(propertiesFilePath)) {
			System.err.println("Properties file " + propertiesFilePath + " not exists!");
			System.exit(-1);
		}		
		
		if (Files.notExists(inspectSourceFileDirPath)) {
			System.err.println("Source directory " + inspectSourceFileDirPath + " not exists!");
			System.exit(-1);
		}
		
		final Path tmpPropFilePath = Paths.get(propertiesFilePath.getParent() + "/tmp-" + propertiesFilePath.getFileName());
		
		try {	
			final Properties prop = loadProperties(propertiesFilePath);			
			copyFile(propertiesFilePath, tmpPropFilePath);
			replaceFileWithPlaceholder(tmpPropFilePath, prop);
			final Properties tmpProp = loadProperties(tmpPropFilePath);	
			
			try (Stream<Path> walk = Files.walk(inspectSourceFileDirPath);){
				List<String> files = new ArrayList<String>();
				if (isExclude) {
					files =	walk.filter(Files::isRegularFile).map(x -> x.toString())
							.filter(f-> !f.matches(excluldeFileRegexString))
							.collect(Collectors.toList());
				}else {
					files =	walk.filter(Files::isRegularFile).map(x -> x.toString())
							.filter(f-> f.matches(incluldeFileRegexString))
							.collect(Collectors.toList());
				}
				
				final long startTimeMillis = System.currentTimeMillis();			
				replaceAllFilesWithPlaceholder(files, tmpProp);
				final long endTimeMillis = System.currentTimeMillis();
				System.out.println("Total Time Taken: " + (endTimeMillis - startTimeMillis) + "ms");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Files.delete(tmpPropFilePath);			
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Properties loadProperties(Path sourceFile) throws IOException{		
		final Charset fileCharset = StandardCharsets.UTF_8;	
		Properties prop = new Properties();
		
		if (Files.notExists(sourceFile)) {
			throw new IOException("Files not exists: " + sourceFile);
		}
		
		try(final BufferedReader bufferedReader = Files.newBufferedReader(sourceFile, fileCharset)){
			prop.load(bufferedReader);
		}
				
		return prop;
	}
	
	public static void copyFile(Path sourceFile, Path destFile) throws IOException {
		if (Files.notExists(sourceFile)) {
			throw new IOException("Files not exists: " + sourceFile);
		}
		
		Files.copy(sourceFile, destFile, StandardCopyOption.REPLACE_EXISTING);
		
	}
	
	public static void replaceFileWithPlaceholder(Path file, Properties props) throws IOException{
		final Charset fileCharset = StandardCharsets.UTF_8;
		Boolean isMatch = false;
		Integer matchCount = 0; 
		String fileContent = new String(Files.readAllBytes(file), fileCharset);
		
		Enumeration<String> propertiesEnumeration = (Enumeration<String>) props.propertyNames(); 
		while (propertiesEnumeration.hasMoreElements()) {
			String keyString = propertiesEnumeration.nextElement();
			String valueString = props.getProperty(keyString); 					
			final String replacePatternString = "\\{" + keyString +"\\}";
							
			if (fileContent.contains(replacePatternString.replaceAll("\\\\", ""))) {
				isMatch = true;
				++matchCount;
			}
			
			fileContent = fileContent.replaceAll(replacePatternString, valueString);
			
		}
		Files.write(file, fileContent.getBytes(fileCharset));
		if (isMatch) System.out.println("Total " + matchCount + " Matches Found in: " +  file);
	}
	
	public static void replaceAllFilesWithPlaceholder(List<String> files, Properties props) throws IOException{
		final Charset fileCharset = StandardCharsets.UTF_8;
		
		for(String file : files) {
			Boolean isMatch = false;
			Path filePath = Paths.get(file);
			String fileContent = new String(Files.readAllBytes(filePath), fileCharset);
			//System.out.println(file);
			
			Enumeration<String> propertiesEnumeration = (Enumeration<String>) props.propertyNames(); 
			while (propertiesEnumeration.hasMoreElements()) {
				String keyString = propertiesEnumeration.nextElement();
				String valueString = props.getProperty(keyString); 					
				final String replacePatternString = "\\{" + keyString +"\\}";
								
				if (fileContent.contains(replacePatternString.replaceAll("\\\\", ""))) {
					isMatch = true;
					//System.out.println(replacePatternString + " pattern found in: " +  file);
				}
				
				fileContent = fileContent.replaceAll(replacePatternString, valueString);
			}
			if (isMatch) {
				Files.write(filePath, fileContent.getBytes(fileCharset));
				System.out.println("Match(es) Found in: " +  file);
			}
			
		}
	}

}
